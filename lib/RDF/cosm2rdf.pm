use strict;
use warnings;

package RDF::cosm2rdf;

use LWP::UserAgent;
use JSON;
use Data::Dumper;
use RDF::Trine;

use Exporter qw(import);
our @EXPORT_OK = qw(getFeedAsJSON describeDatastream describeFeed);

sub getFeedAsJSON {
    my $feedId         = shift;
    my $apiKey         = shift;
    my $cosmAPIBaseURI = "http://api.cosm.com/v2/feeds/";
    my $feedType       = '.json';
    my $feedURI        = $cosmAPIBaseURI . $feedId;
    my $resourceURI    = $feedURI . $feedType;

    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);

    my $req = HTTP::Request->new( GET => $resourceURI );
    $req->header( 'X-ApiKey' => $apiKey );

    my $res  = $ua->request($req);
    my $json = JSON::decode_json( $res->decoded_content );
    return ( json => $json, uri => $feedURI );
}

sub initNodes {
    my %nodes;
    my %baseURIs = (
        ssnxg => 'http://purl.oclc.org/NET/ssnx/ssn#',
        dul   => 'http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#',
        rdf   => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        rdfs  => 'http://www.w3.org/2000/01/rdf-schema#',
    );

    my @ssnXGNodesToAdd = (
        'Sensor',            'SensingDevice',
        'Observation',       'SensorOutput',
        'FeatureOfInterest', 'Property',
        'featureOfInterest', 'madeObservation',
        'observedProperty',  'Platform',
        'attachedSystem',    'hasValue',
        'observationResult', 'ObservationValue'
    );

    addURINodes( \%nodes, $baseURIs{ssnxg}, \@ssnXGNodesToAdd );
    addURINodes( \%nodes, $baseURIs{dul}, ['hasDataValue'] );
    addURINodes( \%nodes, $baseURIs{rdf}, ['type'] );
    return \%nodes;
}

sub addURINodes {
    my $nodes     = shift;
    my $baseURI   = shift;
    my $resources = shift;

    foreach (@$resources) {
        $nodes->{$_} = RDF::Trine::Node::Resource->new( $baseURI . $_ );
    }

    return $nodes;
}

sub describeFeed {
    my $model;
    if ( @_ == 1 ) {
        my $store = RDF::Trine::Store::Memory->new();
        $model = RDF::Trine::Model->new($store);
    }
    else {
        $model = shift;
    }
    my $feed = shift;
    foreach ( @{ $feed->{json}->{datastreams} } ) {
        describeDatastream( $model, $feed->{uri}, $_ );
    }
    return $model;
}

sub describeDatastream {
    my $model;
    if ( @_ == 2 ) {
        my $store = RDF::Trine::Store::Memory->new();
        $model = RDF::Trine::Model->new($store);
    }
    else {
        $model = shift;
    }
    my $feedURI    = shift;
    my $datastream = shift;
    my $id         = $datastream->{id};
    my %nodes      = %{ initNodes() };

    my $sensor = RDF::Trine::Node::Resource->new( $feedURI . '#sensor' . $id );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $sensor, $nodes{type}, $nodes{SensingDevice}
        )
    );
    my $observation =
      RDF::Trine::Node::Resource->new( $feedURI . '#observation' . $id );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $observation, $nodes{type}, $nodes{Observation}
        )
    );
    my $sensorOutput =
      RDF::Trine::Node::Resource->new( $feedURI . '#sensorOutput' . $id );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $sensorOutput, $nodes{type}, $nodes{SensorOutput}
        )
    );
    my $observationValue =
      RDF::Trine::Node::Resource->new( $feedURI . '#observationValue' . $id );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $observationValue, $nodes{type}, $nodes{ObservationValue}
        )
    );
    my $dataValue =
      RDF::Trine::Node::Literal->new( $datastream->{current_value} );

    $model->add_statement(
        RDF::Trine::Statement->new(
            $sensor, $nodes{madeObservation}, $observation
        )
    );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $observation, $nodes{observationResult},
            $sensorOutput
        )
    );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $sensorOutput, $nodes{hasValue}, $observationValue
        )
    );
    $model->add_statement(
        RDF::Trine::Statement->new(
            $observationValue, $nodes{hasDataValue}, $dataValue
        )
    );
    return $model;
}

1;
